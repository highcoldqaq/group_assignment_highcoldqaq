/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Item {
    int productId;
    int salesPrice;
    int quantity;
    String marketSegement;

    public Item(int productId, int salesPrice, int quantity, String marketSegement) {
        this.productId = productId;
        this.salesPrice = salesPrice;
        this.quantity = quantity;
        this.marketSegement = marketSegement;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getMarketSegement() {
        return marketSegement;
    }

    public void setMarketSegement(String marketSegement) {
        this.marketSegement = marketSegement;
    }

    @Override
    public String toString() {
        return "Item{" +
                "productId=" + productId +
                ", salesPrice=" + salesPrice +
                ", quantity=" + quantity +
                ", marketSegement='" + marketSegement + '\'' +
                '}';
    }
}
