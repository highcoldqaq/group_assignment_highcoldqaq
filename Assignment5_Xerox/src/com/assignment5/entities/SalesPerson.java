/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;

/**
 *
 * @author kasai
 */
public class SalesPerson {
    int salesId;
    ArrayList<Order> orders = new ArrayList<>();

    public SalesPerson(int salesId, ArrayList<Order> orders) {
        this.salesId = salesId;
        this.orders = orders;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public void addOrders(Order order) {
        orders.add(order);
    }

    @Override
    public String toString() {
        return "SalesPerson{" +
                "salesId=" + salesId +
                ", orders=" + orders +
                '}';
    }
}
