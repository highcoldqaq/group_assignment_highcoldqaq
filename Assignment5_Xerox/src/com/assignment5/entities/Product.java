/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Product {
    int productId;
    String marketSegement;

    public Product(int productId, String marketSegement) {
        this.productId = productId;
        this.marketSegement = marketSegement;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getMarketSegement() {
        return marketSegement;
    }

    public void setMarketSegement(String marketSegement) {
        this.marketSegement = marketSegement;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", marketSegement='" + marketSegement + '\'' +
                '}';
    }
}
