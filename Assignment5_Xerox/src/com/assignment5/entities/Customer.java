/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;

/**
 *
 * @author kasai
 */
public class Customer {
    int customerId;
    ArrayList<Order> orders = new ArrayList<>();

    public Customer(int customerId, ArrayList<Order> orders) {
        this.customerId = customerId;
        this.orders = orders;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public void addOrders(Order order) {
        orders.add(order);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", orders=" + orders +
                '}';
    }
}
