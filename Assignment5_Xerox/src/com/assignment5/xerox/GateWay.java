/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.controller.DataProcess;
import com.assignment5.controller.DataStore;
import com.assignment5.entities.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author kasai
 */
public class GateWay {

    DataReader orderReader;
    DataReader productReader;
    DataProcess dataProcess;

    GateWay() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        dataProcess = new DataProcess();
    }

    public static void main(String args[]) throws IOException{
        GateWay gw = new GateWay();
        gw.readData();
    }

    private void readData() throws IOException{
        String[] row;
        while ((row = orderReader.getNextRow()) != null) {
            generateCustomer(row);
            generateSalesPeople(row);
            generateOrder(row);
            generateItem(row);
            generateProduct(row);
        }

        dataProcess();
    }

    // Files
    // 0 - Order-Id
    // 1 - Item-id
    // 2 - Product-Id
    // 3 - Quantity
    // 4 - Sales-Id
    // 5 - Customer-Id
    // 6 - Sales-Price-Per-Prod
    // 7 - Market-Segment
    private void generateCustomer(String[] row) {
        int customerId = Integer.valueOf(row[5]);
        DataStore.getInstance().getCustomers().putIfAbsent(customerId, new Customer(customerId, new ArrayList<Order>()));

        Customer c = DataStore.getInstance().getCustomers().get(customerId);

        int orderId = Integer.valueOf(row[0]);
        int customer = Integer.valueOf(row[5]);
        String marketSegment = String.valueOf(row[7]);

        Item item = new Item(Integer.valueOf(row[2]), Integer.valueOf(row[6]), Integer.valueOf(row[3]), marketSegment);
        Order order = new Order(orderId, customer, item);

        c.addOrders(order);
    }

    private void generateOrder(String[] row) {
        int orderId = Integer.valueOf(row[0]);
        int customer = Integer.valueOf(row[5]);
        String marketSegment = String.valueOf(row[7]);
        Item item = new Item(Integer.valueOf(row[2]), Integer.valueOf(row[6]), Integer.valueOf(row[3]), marketSegment);
        Order order = new Order(orderId, customer, item);

        DataStore.getInstance().getOrders().put(orderId, order);
    }

    private void generateItem(String[] row) {
        int productId = Integer.valueOf(row[2]);
        int salesPrice = Integer.valueOf(row[6]);
        int quantity = Integer.valueOf(row[3]);
        String marketSegment = String.valueOf(row[7]);

        DataStore.getInstance().getItems().put(productId, new Item(productId, salesPrice, quantity, marketSegment));
    }

    private void generateProduct(String[] row) {
        int productId = Integer.valueOf(row[2]);
        String marketSegment = String.valueOf(row[7]);

        DataStore.getInstance().getProducts().put(productId, new Product(productId, marketSegment));
    }

    private void generateSalesPeople(String[] row) {
        int salesId = Integer.valueOf(row[4]);
        DataStore.getInstance().getSalesPeople().putIfAbsent(salesId, new SalesPerson(salesId, new ArrayList<Order>()));
        SalesPerson saler = DataStore.getInstance().getSalesPeople().get(salesId);
        String marketSegment = String.valueOf(row[7]);

        int orderId = Integer.valueOf(row[0]);
        int customer = Integer.valueOf(row[5]);
        Item item = new Item(Integer.valueOf(row[2]), Integer.valueOf(row[6]), Integer.valueOf(row[3]), marketSegment);
        Order order = new Order(orderId, customer, item);

        saler.addOrders(order);
    }


    private void dataProcess() {
        dataProcess.top5PopProduct();
        dataProcess.topCustomersByMarket();
        dataProcess.top5Customers();
        dataProcess.best3SalesPeople();
        dataProcess.totalRevenue();
        dataProcess.revenueByMarkets();
    }
}
