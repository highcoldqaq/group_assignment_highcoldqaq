package com.assignment5.controller;

import com.assignment5.entities.*;

import java.util.*;

/**
 * @Description:
 * @Author: Renzi Meng
 * @Date: 2019/3/4 13:45
 * @Copyright (C):, 2019, Renzi Meng, All Rights Reserved.
 */

public class DataProcess {
    // 1. Our top 5 most popular product sorted from high to low.
    public void top5PopProduct() {
        Map<Integer, Item> itemsMap = DataStore.getInstance().getItems();
        Map<Integer, Integer> countsProduct = new HashMap<>();

        for (Item item : itemsMap.values()) {
            int productId = item.getProductId();
            int quantity = item.getQuantity();

            int sum = countsProduct.getOrDefault(productId, 0) + quantity;
            countsProduct.put(productId, sum);
        }

        Queue<Map.Entry<Integer, Integer>> pq = new PriorityQueue<>(
                new Comparator<Map.Entry<Integer, Integer>>() {
                    @Override
                    public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                        return o2.getValue() - o1.getValue();
                    }
                }
        );

        pq.addAll(countsProduct.entrySet());

        System.out.println("Our top 5 most popular product sorted from high to low.");
        for (int i = 0; i < 5; i++) {
            Map.Entry<Integer, Integer> entry = pq.poll();
            System.out.println(i + ". Product Id: "  + entry.getKey() + "\n" + " the numbers of it sales: " + entry.getValue());
        }

    }

    // 2. Our top customers in certain markets (e.g., education market, financial market, retail, etc.)
    public void topCustomersByMarket() {
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Set<String> set = new HashSet<>();
        Map<Map<String, Integer>, Integer> countSales = new HashMap<>();

        for (Order order: orders.values()) {
            int customerId = order.getCustomerId();
            int quantity = order.getItem().getQuantity();
            String market = order.getItem().getMarketSegement();
            HashMap<String, Integer> map = new HashMap<>();
            map.put(market, customerId);
            countSales.put(map, countSales.getOrDefault(map, 0) + quantity);
        }

        Queue<Map.Entry<Map<String, Integer>, Integer>> pq = new PriorityQueue<>(
                new Comparator<Map.Entry<Map<String, Integer>, Integer>>() {
                    @Override
                    public int compare(Map.Entry<Map<String, Integer>, Integer> o1, Map.Entry<Map<String, Integer>, Integer> o2) {
                        return - o1.getValue() + o2.getValue();
                    }
                }
        );

        pq.addAll(countSales.entrySet());
        System.out.println("\n Our top customers in certain markets (e.g., education market, financial market, retail, etc.)");

        while (!pq.isEmpty()) {
            Map.Entry<Map<String, Integer>, Integer> entrys = pq.poll();
            for (Map.Entry<String, Integer> entry : entrys.getKey().entrySet()) {
                if (!set.contains(entry.getKey())) {
                    System.out.println("top customers:" + entry.getValue() + " buy " +  entrys.getValue() + " in "+ entry.getKey());
                    set.add(entry.getKey());
                }
            }
        }
    }

    // 3. 5 best customers.
    public void top5Customers() {
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Integer> countSales = new HashMap<>();

        for (Order order : orders.values()) {
            int customerId = order.getCustomerId();
            int quanity = order.getItem().getQuantity();

            countSales.put(customerId, quanity);
        }

        PriorityQueue<Map.Entry<Integer, Integer>> pq = new PriorityQueue<>(
                new Comparator<Map.Entry<Integer, Integer>>() {
                    @Override
                    public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                        return - o1.getValue() + o2.getValue();
                    }
                }
        );


        System.out.println("\n 5 best customers.");
        pq.addAll(countSales.entrySet());

        for (int i = 0; i< 5; i++) {
            Map.Entry<Integer, Integer> entry = pq.poll();
            System.out.println("Customer Id: " + entry.getKey() + " buy " + entry.getValue());
        }
    }

    // 4. Our top 3 best sales people.
    public void best3SalesPeople(){
        Map<Integer, SalesPerson> salesPeople = DataStore.getInstance().getSalesPeople();
        Map<Integer,Integer> sales = new HashMap<>();
        for(SalesPerson sp: salesPeople.values()){
            int sum = 0;
            ArrayList<Order> orders = sp.getOrders();
            for(Order order:orders){
                Item item = order.getItem();
                sum += item.getQuantity() * item.getSalesPrice();
            }
            sales.put(sp.getSalesId(), sum);
        }
        
        ArrayList<Map.Entry<Integer,Integer>> list = new ArrayList<>(sales.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return -o1.getValue().compareTo(o2.getValue());
            }
        });
        
        System.out.println("\n Best 3 sales people.");
        Iterator<Map.Entry<Integer,Integer>> iterator = list.iterator();
        int i = 1;
        for(Map.Entry<Integer,Integer> m : list){
            if(i>3){
                break;
            }
            System.out.println("Sales person with id:"+m.getKey()+" sales "+m.getValue());
            i++;
        }

    }
    // 5. Our total revenue for the year.
    public void totalRevenue(){
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        int revenue = 0;
        for(Order o: orders.values()){
            Item i = o.getItem();
            revenue += i.getQuantity() * i.getSalesPrice();
        }
        System.out.println("\n Our total revenue:"+revenue);
            
    }
    // 6. 6 total revenues by markets.
    public void revenueByMarkets(){
        Map<String,Integer> rByMarkets = new HashMap<>();
        rByMarkets.put("retail", 0);
        rByMarkets.put("education", 0);
        rByMarkets.put("software", 0);
        rByMarkets.put("realestate", 0);
        rByMarkets.put("financial", 0);
        rByMarkets.put("pharmaceutical", 0);
        
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        for(Order o:orders.values()){
            Item i = o.getItem();
            int value = rByMarkets.get(i.getMarketSegement());
            value = value + i.getQuantity()*i.getSalesPrice();
            rByMarkets.put(i.getMarketSegement(),value);
        }
        System.out.println("\n 6 total revenue by markets.");
        Iterator iter = rByMarkets.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			Object key = entry.getKey();
			Object value = entry.getValue();
			System.out.println(key + ":" + value);
 
		}

    }
    
}
