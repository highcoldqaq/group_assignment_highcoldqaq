/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;

/**
 *
 * @author AEDSpring2019
 */
public class Customer extends User implements Comparable<Supplier>{
    private Date createTime;

    public Customer(String password, String userName, Date createTime) {
        super(password, userName, "CUSTOMER");
        this.createTime = createTime;

    }

    public Date getCreateTime() {
        return createTime;
    }
    

    @Override
    public boolean verify(String password) {
        if(password.equals(getPassword()))
            return true;
        return false;
    }

    @Override
    public int compareTo(Supplier t) {
        return t.getUserName().compareTo(this.getUserName());
    }
    
}
