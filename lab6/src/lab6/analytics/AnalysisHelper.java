/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6.analytics;

import java.util.*;

import javafx.geometry.Pos;
import lab6.entities.Comment;
import lab6.entities.Post;
import lab6.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    //  key: UserId ; Value: sum of likes from all comments
    public void userWithMostLikes() {
        Map<Integer, Integer> userLikesCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
    
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikesCount.containsKey(user.getId())) {
                    likes = userLikesCount.get(user.getId());
                }
                likes += c.getLikes();
                userLikesCount.put(user.getId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for (int id : userLikesCount.keySet()) {
            if (userLikesCount.get(id) > max) {
                max = userLikesCount.get(id);
                maxId = id;
            }
        }
        System.out.println("User with most likes: " + max + "\n" 
            + users.get(maxId));
    }
    
    
    public void getFiveMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>() {
            @Override 
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
            }
        });
        
        System.out.println("5 most likes comments: ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
        }
    }
    
    // Find Average number of likes per comment.
    public void getAverageComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        int likeSum = 0;
        int commentNum=0; 
        for(Comment c : comments.values()){
            commentNum++;
            likeSum = likeSum + c.getLikes();
        }
        int averageLike = likeSum/commentNum;
        System.out.println("Average number of likes per comment:");
        System.out.println(averageLike);
    } 
    
    // Find the post with most Liked Comments    
    public void getPostsByMostLikedComments() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        int maxLikes = 0;
        Post postOfMostLiked = null;
        Comment commentOfMostLiked = null;
        for(Post post : posts.values()){
            
            for(Comment c : post.getComments()){
                if (c.getLikes()> maxLikes){
                maxLikes = c.getLikes();
                postOfMostLiked = post;
                commentOfMostLiked = c;
            }
            }
            
        }
        System.out.println("The post with most liked comment:");
        System.out.println(postOfMostLiked+"with a most liked comment :"+commentOfMostLiked);
    
    }
    
    // Find the post with most Comments
    public void getPostsByMostComments() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Post result = null;
        int MaxComment = 0;
        for(Post p : posts.values()){
            if (p.getComments().size()>MaxComment){
                MaxComment = p.getComments().size();
                result = p;
            }
        }
        System.out.println("The post with most comments:");
        System.out.println(result);
    
    }

    // Top 5 Inactive users base on total posts number
    public void getFiveMostInactiveUsersByPosts() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        HashMap<User, HashSet<Integer>> countPost = new HashMap<>();
        
        PriorityQueue<Map.Entry<User, HashSet<Integer>>> pq = new PriorityQueue<> (
                new Comparator<Map.Entry<User, HashSet<Integer>>>() {
                    @Override
                    public int compare(Map.Entry<User, HashSet<Integer>> o1, Map.Entry<User, HashSet<Integer>> o2) {
                        return o1.getValue().size() - o2.getValue().size();
                    }
                });

        for (Post post : posts.values()) {
            countPost.putIfAbsent(users.get(post.getUserId()), new HashSet<Integer>());
            countPost.get(users.get(post.getUserId())).add(post.getPostId());
        }

        pq.addAll(countPost.entrySet());

        System.out.println("Top 5 Inactive users base on total posts number: ");
        for (int i = 0; i< 5; i++) {
            Map.Entry<User, HashSet<Integer>> entry = pq.poll();
            System.out.println(entry.getKey() + " with " + entry.getValue().size() + " posts.");
        }
    }
    
    // Top 5 Inactive users based on total comments they created
    public void getFiveMostInactiveUsersByComments() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        HashMap<User, Integer> countComments = new HashMap<>();
        
        for (User user : users.values()) {
            countComments.put(user, user.getComments().size());
        }
        
        PriorityQueue<Map.Entry<User, Integer>> pq = new PriorityQueue<> (
            new Comparator<Map.Entry<User, Integer>>() {
                @Override
                public int compare(Map.Entry<User, Integer> o1, Map.Entry<User, Integer> o2) {
                    return o1.getValue() - o2.getValue();
                }
            }
        );
            
        pq.addAll(countComments.entrySet());
        
        System.out.println("Top 5 Inactive users based on total comments they created");
        for (int i = 0; i< 5; i++) {
            Map.Entry<User, Integer> entry = pq.poll();
            System.out.println(entry.getKey() + " with " + entry.getValue() + " comments.");
        }
        
    }
    
    // Top 5 Inactive users overall (Sum of Comments, posts and likes)
    public void getFiveMostInactiveUsersByOverall () {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<User, Integer> overAllSum = new HashMap<>();

        PriorityQueue<Map.Entry<User, Integer>> pq = new PriorityQueue<> (
                new Comparator<Map.Entry<User, Integer>>() {
                    @Override
                    public int compare(Map.Entry<User, Integer> o1, Map.Entry<User, Integer> o2) {
                        return o1.getValue() - o2.getValue();
                    }
                }
        );

        for (User user : users.values()) {
            overAllSum.put(user, user.getComments().size());
        }

        for (Comment c : comments.values()) {
            overAllSum.put(users.get(c.getUserId()), overAllSum.getOrDefault(users.get(c.getUserId()), 0) + c.getLikes());
        }

        HashMap<User, HashSet<Integer>> countPost = new HashMap<>();

        for (Post post : posts.values()) {
            countPost.putIfAbsent(users.get(post.getUserId()), new HashSet<Integer>());
            countPost.get(users.get(post.getUserId())).add(post.getPostId());
        }

        for (Map.Entry<User, HashSet<Integer>> entry : countPost.entrySet()) {
            overAllSum.put(entry.getKey(), overAllSum.getOrDefault(entry.getKey(), 0) + entry.getValue().size());
        }

        pq.addAll(overAllSum.entrySet());

        System.out.println("Top 5 Inactive users overall (Sum of Comments, posts and likes)");
        for (int i = 0; i< 5; i++) {
            Map.Entry<User, Integer> entry = pq.poll();
            System.out.println(entry.getKey() + " with " + entry.getValue() + " overAll Sum");
        }
    }
    
    // Top 5 proactive users overall (sum of comments, posts and likes)
    public void getFiveMostProactiveUsersByOverall () {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<User, Integer> overAllSum = new HashMap<>();

        PriorityQueue<Map.Entry<User, Integer>> pq = new PriorityQueue<> (
                new Comparator<Map.Entry<User, Integer>>() {
                    @Override
                    public int compare(Map.Entry<User, Integer> o1, Map.Entry<User, Integer> o2) {
                        return o2.getValue() - o1.getValue();
                    }
                }
        );

        for (User user : users.values()) {
            overAllSum.put(user, user.getComments().size());
        }

        for (Comment c : comments.values()) {
            overAllSum.put(users.get(c.getUserId()), overAllSum.getOrDefault(users.get(c.getUserId()), 0) + c.getLikes());
        }

        HashMap<User, HashSet<Integer>> countPost = new HashMap<>();

        for (Post post : posts.values()) {
            countPost.putIfAbsent(users.get(post.getUserId()), new HashSet<Integer>());
            countPost.get(users.get(post.getUserId())).add(post.getPostId());
        }

        for (Map.Entry<User, HashSet<Integer>> entry : countPost.entrySet()) {
            overAllSum.put(entry.getKey(), overAllSum.getOrDefault(entry.getKey(), 0) + entry.getValue().size());
        }

        pq.addAll(overAllSum.entrySet());

        System.out.println("Top 5 Proactive users overall (Sum of Comments, posts and likes)");
        for (int i = 0; i< 5; i++) {
            Map.Entry<User, Integer> entry = pq.poll();
            System.out.println(entry.getKey() + " with " + entry.getValue() + " overAll Sum");
        }
    }
}
